package yuliia.bank.dataSource.network.dto

import yuliia.bank.model.Bank

data class BankList(
    val results: Collection<Bank>,
)
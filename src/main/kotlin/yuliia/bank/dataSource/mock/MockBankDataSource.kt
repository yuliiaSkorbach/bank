package yuliia.bank.dataSource.mock

import org.springframework.stereotype.Repository
import yuliia.bank.dataSource.BankDataSource
import yuliia.bank.model.Bank

@Repository("mock")
class MockBankDataSource : BankDataSource {
    val banks = mutableListOf(
        Bank("1234", 3, 1),
        Bank("1204", 7, 1),
        Bank("8234", 3, 1)
    )

    override fun retrieveBanks(): Collection<Bank> = banks
    override fun retrieveBanks(accountNumber: String): Bank {
        return banks.firstOrNull() {
            it.accountNumber == accountNumber
        } ?: throw NoSuchElementException("Could not find a bank with account number $accountNumber")
    }

    override fun createBank(bank: Bank): Bank {
        if (banks.any {
                it.accountNumber == bank.accountNumber
            }) {
            throw IllegalArgumentException("Bank with account number ${bank.accountNumber} already exists")
        }
        banks.add(bank)
        return bank
    }

    override fun updateBank(bank: Bank): Bank {
        val currentBank = banks.firstOrNull { it.accountNumber == bank.accountNumber }
            ?: throw NoSuchElementException("Could not find a bank with account number ${bank.accountNumber}")

        banks.remove(currentBank)
        banks.add(bank)

        return bank
    }

    override fun deleteBank(accountNumber: String) {
        val currentBank = banks.firstOrNull { it.accountNumber == accountNumber }
            ?: throw NoSuchElementException("Could not find a bank with account number ${accountNumber}")

        banks.remove(currentBank)
    }


}
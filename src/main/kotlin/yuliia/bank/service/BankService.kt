package yuliia.bank.service

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import yuliia.bank.dataSource.BankDataSource
import yuliia.bank.model.Bank

@Service
class BankService(@Qualifier("mock") private val dataSource: BankDataSource) {
    fun getBanks(): Collection<Bank> {
        return dataSource.retrieveBanks()
    }

    fun getBanks(accountNumber: String): Bank {
        return dataSource.retrieveBanks(accountNumber)
    }

    fun addBank(bank: Bank): Bank {
        return dataSource.createBank(bank)
    }

    fun updateBank(bank: Bank): Bank {
        return dataSource.updateBank(bank)
    }

    fun deleteBank(accountNumber: String) {
        return dataSource.deleteBank(accountNumber)
    }
}
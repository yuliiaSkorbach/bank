package yuliia.bank.dataSource.mock

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MockBankDataSourceTest {
    private val mockBankDataSource = MockBankDataSource()

    @Test
    fun `should provide a collections of banks`() {
        //when
        val bank = mockBankDataSource.retrieveBanks()


        assertThat(bank.size).isGreaterThanOrEqualTo(3)
    }

    @Test
    fun `should provide some mock data`() {

        //when
        val banks = mockBankDataSource.retrieveBanks()

        //then
        assertThat(banks).allMatch { it.accountNumber.isNotBlank() }
        assertThat(banks).anyMatch { it.trust != 0 }
        assertThat(banks).anyMatch { it.transactionFree != 0 }
    }
}